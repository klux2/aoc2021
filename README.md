# Advent of Code 2021

This repository contains inputs and their corresponding outputs for some days of the "[Advent of Code 2021](https://adventofcode.com/2021)" challenge.

It also contains a Python 3 template that will open a data file and parse each line a an integer, when executed.

For questions regarding how to solve the challenges, please visit https://reddit.com/r/AdventOfCode
![https://reddit.com/r/AdventOfCode](qr_reddit.png)