#!/usr/bin/env python3

from typing import List

data_file_name = "data.txt"

def read_data(filename: str) -> List[int]:
    with open(filename) as f:
        lines = f.readlines()

        data = []
        for line in lines:
            data.append(int(line))

        # shorter:
        #data = [int(line) for line in lines]
        # or
        #data = list(map(int, lines))

    return data

def process(data: List[int]):
    pass

def main():
    data = read_data(data_file_name)
    process(data)

if __name__ == "__main__":
    main()
